import { Container, Row, Col, Input, Label, Button } from "reactstrap";

function FormRegister () {
    return (
        <Container style={{width:'1000px'}} className="bg-light border mt-5 mb-5" fluid='sm'>
            <Row className="text-center mt-3">
                <Col>
                <h1>Register form</h1>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col xs={2} className="text-center">
                    <Label>First Name <span className="text-danger">(*)</span></Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
                <Col xs={2} className="text-center">
                <Label>Passport <span className="text-danger">(*)</span></Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col xs={2} className="text-center">
                    <Label>Last Name <span className="text-danger">(*)</span></Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
                <Col xs={2} className="text-center">
                <Label>Email</Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col xs={2} className="text-center">
                    <Label>Birth Day <span className="text-danger">(*)</span></Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
                <Col xs={2} className="text-center">
                <Label>Country <span className="text-danger">(*)</span></Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
            </Row>  
            <Row className="mt-2">
                <Col xs={2} className="text-center">
                    <Label>Gender <span className="text-danger">(*)</span></Label>
                </Col>
                <Col xs={4}>
                    <Row>
                    <Col>
                        <Input name="radio1" type="radio"/>
                        <Label check>
                            Male
                        </Label>
                    </Col>
                    <Col>
                        <Input name="radio2" type="radio"/>
                        <Label check>
                            Female
                        </Label>
                    </Col>     
                    </Row>                                
                </Col>
                <Col xs={2} className="text-center">
                <Label>Region</Label>
                </Col>
                <Col xs={4}>
                    <Input/>
                </Col>
            </Row>          
            <Row className="mt-2">
                <Col xs={2} className="text-center">
                    <Label>Subject</Label>
                </Col>
                <Col>
                    <Input type="textarea"></Input>
                </Col>
            </Row>
            <Row className="mb-5">
                <Col className="text-center mt-4">
                    <Button place color="danger">Check Data</Button>
                    {' '}
                    <Button color="info">Send</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default FormRegister;